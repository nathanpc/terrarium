<?php 
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	include '../scripts/identica.lib.php';
	include '../scripts/linkify.php';
	include '../misc.php';

	// your identi.ca username and password
	$username = base64_decode($_GET['u']);
	$password = base64_decode($_GET['p']);

	// initialize the identi.ca class
	$identica = new Identica($username, $password, "Terrarium");

	// fetch the timeline in xml format
	$options = array("count" => 20);
	$xml = $identica->getFollowers("xml");

	$identica_status = new SimpleXMLElement($xml);
	foreach($identica_status->user as $user) {
		echo '<li class="post"><img src="' . $user->profile_image_url . '" class="identica_image">';
		echo '<a href="http://identi.ca/' . $user->screen_name . '" class="nameURL">' . $user->name . '</a>';
		echo linkify($user->description);
		echo '<br/>';
		echo '<div class="identica_posted_at"> Posts: ' . $user->statuses_count . ' &#149; Subscribers: ' . $user->followers_count . '</div>';
		echo "</li>";
	}
?>