<?php 
	ini_set('display_errors', 1);
	error_reporting(E_ALL);

	include '../scripts/identica.lib.php';
	include '../scripts/linkify.php';
	include '../misc.php';

	// your identi.ca username and password
	$username = base64_decode($_GET['u']);
	$password = base64_decode($_GET['p']);
	$status = $_GET['s'];
	//$timezone = $_GET['t'];

	// initialize the identi.ca class
	$identica = new Identica($username, $password, "Terrarium");

	// fetch the timeline in xml format
	$xml = $identica->updateStatus($status);
?>