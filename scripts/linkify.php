<?php

	function linkify_social($post) {
	    $post = preg_replace('/(^|\s)@(\w+)/', '\1<a href="http://identi.ca/\2">@\2</a>', $post);
	    return preg_replace('/(^|\s)#(\w+)/', '\1<a href="http://identi.ca/tag/\2">#\2</a>', $post);
	}
	
	function linkify_groups($post) {
		$post = preg_replace('/(^|\s)!(\w+)/', '\1<a href="http://identi.ca/group/\2">!\2</a>', $post);
		return linkify_social($post);
	}

	function linkify($text) {
		$text = preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', '<a href="$1">$1</a>', $text);
		return linkify_groups($text);
	}

?>