var map = null;

function pageLoaded() {
	addEventListeners();
	orientationChanged();
	getTweets();
}

function orientationChanged() {
	var header = document.getElementById("head");
	
	if((window.orientation == 90) || (window.orientation == -90)) {
		navigator.geolocation.getCurrentPosition(loadMap, function(error) {
			console.log(error.message);
		});
		
		header.innerText = "Map View";
	} else {
		getTweets();
		header.innerText = "Terrarium";
	}
}

function loadMap(position) {
	var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	
	var myOptions = {
		center: latLng,
		zoom: 8,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);
	
	loadLocalTweets(position);
}

function loadLocalTweets(position) {
	var client = new XMLHttpRequest();
	client.onreadystatechange = function() {
		if (this.readyState == this.DONE && this.status == 200) {
			var response = eval('(' + this.responseText + ')');
			var re = new RegExp('-?\\d+\\.\\d+\\,-?\\d+\\.\\d+');
			
			for (var resultId in response.results) {
				var result = response.results[resultId];
				
				var m = re.exec(result.location);
				if ((m) && (m.length > 0)) {
					var latLng = m[0].split(',');
					var location = new google.maps.LatLng(latLng[0], latLng[1]);
					try {
						var mark = new google.maps.Marker({
							position: location,
							map: map
						});
					}
					catch (ex) {
						console.log(ex);
					}
				}
			}
		}
	};
	var apiCall = 'http://search.twitter.com/search.json?rpp=100&geocode=' + position.coords.latitude + ',' + position.coords.longitude + ',25mi';
	var requestString = '../../../shared/scripts/curl.php?apiCall=' + escape(apiCall);
	
	client.open('GET', requestString, true);
	client.send();
}

function getTweets() {
	var client = new XMLHttpRequest();
	client.onreadystatechange = function() {
		if (this.readyState == XMLHttpRequest.prototype.DONE && this.status == 200) {
			console.log("Got tweets? Yup!");
			
			var response = eval("(" + this.responseText + ")");
			// TODO: Loop through results and Create List Items for our list.
			for(var resultId in response.results) {
				var result = response.results[resultId];
				
				var name = result.from_user;
				var text = result.text;
				var imageUrl = result.profile_image_url;
				
				var imageElement = document.createElement("img");
				imageElement.setAttribute("src", imageUrl);
				imageElement.setAttribute("alt", "Avatar " + name);
				imageElement.setAttribute("class", "user-image");
				imageElement.setAttribute("onClick", "showProfile('" + name + "')");
				
				var usernameSpan = document.createElement("span");
				usernameSpan.setAttribute("class", "username");
				usernameSpan.innerText = name;
				
				var messageSpan = document.createElement("span");
				messageSpan.setAttribute("class", "message");
				messageSpan.innerText = text;
				
				var li = document.createElement("li");
				li.appendChild(imageElement);
				li.appendChild(usernameSpan);
				li.appendChild(messageSpan);
				
				var tweetsList = document.getElementById("tweetsList");
				tweetsList.insertBefore(li, tweetsList.firstChild);
			}
		}
	};
	var apiCall = 'http://search.twitter.com/search.json?q=OReillyMedia';
	var requestString = 'scripts/curl.php?apiCall=' + escape(apiCall);

	client.open('GET', requestString, true);
	client.send();
}

function getProfileForScreenName(screen) {
	var client = new XMLHttpRequest();
	client.onreadystatechange = function() {
		if(this.readyState == XMLHttpRequest.prototype.DONE && this.status) {
			var profile = eval('(' + this.responseText + ')');
			
			// Get HTML Elements.
			var profileImage = document.getElementById('profileImage');
			profileImage.setAttribute('src', profile.profile_image_url);
			profileImage.setAttribute('alt', profile.name);
			
			var name = document.getElementById('name');
			name.innerText = profile.name;
			
			var screenName = document.getElementById('screenName');
			screenName.innerText = "@" + profile.screen_name;
			
			var location = document.getElementById('location');
			location.innerText = profile.location;
			
			var statusText = document.getElementById('statusText');
			statusText.innerText = profile.status.text;
			
			var followersCount = document.getElementById('followersCount');
			followersCount.innerText = "Followers: " + profile.followers_count;
			
			var friendsCount = document.getElementById('friendsCount');
			friendsCount.innerText = "Following: " + profile.friends_count;
			
			switchToSectionWithId('profile');
		}
	};
	var apiCall = 'http://api.twitter.com/1/users/show.json?screen_name=' + screen;
	var requestString = 'scripts/curl.php?apiCall=' + escape(apiCall);

	client.open('GET', requestString, true);
	client.send();
}

function showProfile(username) {
	clearAllNavItems();
	getProfileForScreenName(username);
}