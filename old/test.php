<link rel="stylesheet" href="style.css" type="text/css" media="screen" title="no title" charset="utf-8">

<script src="scripts/jquery.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
	$("#twitterTimeline").css("width", getViewport("width") + "px");
	
	function getViewport(view) {
		var viewportWidth;
		var viewportHeight;

		if(typeof window.innerWidth != "undefined") {
			viewportWidth = window.innerWidth;
			viewportHeight = window.innerHeight;
		}

		if(view = "width") {
			return viewportWidth;
		} else {
			return viewportHeight;
		}
	}
</script>

<div id="twitterTimeline">
	<ul id="tweetsList" class="tweetsList">
		<?php 
			// require the twitter library
			require "scripts/twitter.lib.php";

			// your twitter username and password
			$username = "nathanpc";
			$password = "nathan";

			// initialize the twitter class
			$twitter = new Twitter($username, $password);

			// fetch public timeline in xml format
			//$options = array("count" => 20);
			//$xml = $twitter->getFriendsTimeline($options);
			$xml = $twitter->getPublicTimeline();

			$twitter_status = new SimpleXMLElement($xml);
			foreach($twitter_status->status as $status) {
				foreach($status->user as $user) {
						echo '<li class="tweet"><img src="' . $user->profile_image_url . '" class="twitter_image">';
						echo '<a href="http://www.twitter.com/' . $user->name . '">' . $user->name . '</a>: ';
				}
				echo $status->text;
				echo '<br/>';
				echo '<div class="twitter_posted_at">Posted at: ' . $status->created_at . '</div>';
				echo "</li>";
			}
		?>
	</ul>
<div>