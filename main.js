var homePage = "tabs/home.php?u=" + Base64.encode("nathanpc") + "&p=" + Base64.encode("e45r6tgyju");
var mentionPage = "tabs/mentions.php?u=" + Base64.encode("nathanpc") + "&p=" + Base64.encode("e45r6tgyju");
var messagePage = "tabs/messages.php?u=" + Base64.encode("nathanpc") + "&p=" + Base64.encode("e45r6tgyju");
var followPage = "tabs/followers.php?u=" + Base64.encode("nathanpc") + "&p=" + Base64.encode("e45r6tgyju");
var postPage = "tabs/post.php?u=" + Base64.encode("nathanpc") + "&p=" + Base64.encode("e45r6tgyju") + "&s=";

// Timezone try: var homePage = "tabs/home.php?u=" + Base64.encode("nathanpc") + "&p=" + Base64.encode("e45r6tgyju") + "&t=America/Sao_Paulo";

// TODO:
// Implement the release to refresh
// Put the loading image while posting
// Press Enter and it will post
// Im plement a login screen and a logout button
// I think this is all..

var scrollNav, scrollMent, scrollMsg;
var postBox = null;

function pageLoaded() {
	window.scrollTo(0, 0);
	
	centerLoading();
	showLoading();

	switchToSectionWithId('identicaTimeline');
	$("#identicaHome").load(homePage, function() {
		$("#timelineScroller").append("<div id='scrollSpace'></div>");
		scrollNav = new iScroll('identicaTimeline', { vScrollbar: false, hideScrollbar: true, fadeScrollbar: true });
		hideLoading();
	});
	
	setLeftHidden("#postBox");
	addEventListeners();
}

function getViewport(view) {
	var viewportWidth;
	var viewportHeight;

	if(typeof window.innerWidth != "undefined") {
		viewportWidth = window.innerWidth;
		viewportHeight = window.innerHeight;
	}
	
	if(view = "width") {
		return viewportWidth;
	} else {
		return viewportHeight;
	}
}

function exclude(num, minus) {
	return num - minus;
}

function addEventListeners() {
	/* Orientation Change
	var supportsOrientationChange = "onorientationchange" in window;
	var orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
	
	window.addEventListener(orientationEvent, orientationChanged, false); */
	
	document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
}

function navSelected() {
	// First we need to clear all other nav items.
	clearAllNavItems();
	
	// Now give navItem the selected css class.
	var navItem = event.target;
	navItem.setAttribute('class', 'selected');
	
	// Now do the right thing.
	switch (navItem.id) {
		case 'liHome':
			showLoading();
			switchToSectionWithId('identicaTimeline');
			$("#identicaHome").load(homePage, function() {
				$("#timelineScroller").append("<div id='scrollSpace'></div>");
				scrollNav = new iScroll('identicaTimeline', { vScrollbar: false, hideScrollbar: true, fadeScrollbar: true });
				hideLoading();
			});
			break;
		case 'liMentions':
			showLoading();
			switchToSectionWithId('identicaMentions');
			$("#identicaMent").load(mentionPage, function() {
				$("#mentionScroller").append("<div id='scrollSpace'></div>");
				
				scrollMent = new iScroll('identicaMentions', { vScrollbar: false, hideScrollbar: true, fadeScrollbar: true });
				hideLoading();
			});
			break;
		case 'liMessages':
			showLoading();
			switchToSectionWithId('identicaMessages');
			$("#identicaMsg").load(messagePage, function() {
				$("#messageScroller").append("<div id='scrollSpace'></div>");
				
				scrollMsg = new iScroll('identicaMessages', { vScrollbar: false, hideScrollbar: true, fadeScrollbar: true });
				hideLoading();
			});
			break;
		case 'liFollow':
			showLoading();
			switchToSectionWithId('identicaFollowers');
			$("#identicaFollow").load(followPage, function() {
				$("#followScroller").append("<div id='scrollSpace'></div>");
				
				scrollMsg = new iScroll('identicaFollowers', { vScrollbar: false, hideScrollbar: true, fadeScrollbar: true });
				hideLoading();
			});
			break;
		case 'liMore':
			switchToSectionWithId('identicaMore');
			break;
	}
}

function clearAllNavItems() {
	var navList = document.getElementById('navList');
	
	for (var i = 0; i < navList.children.length; i++) {
		var li = navList.children[i];
		li.setAttribute('class', '');
	}
}

function switchToSectionWithId(sectionId) {
	hideAllSections();
	showSectionWithId(sectionId);
}

function hideAllSections() {
	var sections = document.getElementsByTagName('section');
	for (var i = 0; i < sections.length; i++) {
		var section = sections[i];
		section.setAttribute('class', 'hidden');
	}
}

function showSectionWithId(sectionId) {
	var section = document.getElementById(sectionId);
	section.setAttribute('class', 'selected');
}

function textCounter(field, cntField, maxLimit) {
	cntField.innerHTML = maxLimit - field.value.length;
	
	if (field.value.length > 120) {
		$("#label").css("color", "#ff0034");
	} else if (field.value.length > 100) {
		$("#label").css("color", "#ff6025");
	} else {
		$("#label").css("color", "#3e3e3e");
	}
}

function setLeftHidden(element) {
	var startPos = $(window).width() - 2 * $(window).width();
	$(element).css("left", startPos);
}

function showPostBox() {
	$("#postBox").removeClass("hidden");
	$("#postBox").animate({"left": "+=" + getViewport("width")}, "slow");
}

function hidePostBox() {
	$("#postBox").animate({"left": "-=" + getViewport("width")}, "slow", function() {
		$("#postBox").addClass("hidden")
	});
}

function createPost_Click() {
	postBox = $("#postBox").hasClass("hidden");
	
	if (postBox === true) {
		showPostBox();
	} else {
		hidePostBox();
	}
}

function btPost_Click() {
	$.post(postPage + document.postBox.postInput.value, function(data) {
	  alert(data);
	});
}

function centerLoading() {
	var center = window.innerWidth / 2 - 23;
	var centerb = window.innerWidth / 2 - 28;
	var centerb_h = window.innerHeight / 2 - 5;

	$("#loadingBkg").css("top", centerb_h + "px");
	$("#loadingBkg").css("left", centerb + "px");
	$("#loading").css("left", center + "px");
}

function hideLoading() {
	$("#loadingBkg").fadeOut("slow")
	$("#loading").fadeOut("slow")
}

function showLoading() {
	$("#loadingBkg").fadeIn("slow")
	$("#loading").fadeIn("slow")
}